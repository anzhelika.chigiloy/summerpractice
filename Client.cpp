#include <iostream>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <string>
#include <regex>
#include <ctime>
#include <conio.h>
#pragma comment (lib, "Ws2_32.lib")
using namespace std;
const short serverPort = 1234;
const short clientPort = 1235;

void initializeLibrary()
{
	WSADATA wsaData;
	int errorCode = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (errorCode != 0)
	{
		cout << "Error during initialization!" << endl;
		system("pause");
		exit(1);
	}
}

SOCKET createSocket()
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		cout << "Error during socket creation!" << endl;
		system("pause");
		exit(1);
	}
	return sock;
}

void bindSocket(SOCKET sock)
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_port = clientPort + rand() % 100;
	address.sin_addr.s_addr = 0; // �� ��������� IP �����
	int errorCode = bind(sock, (sockaddr*)&address, sizeof(address));
	if (errorCode != 0)
	{
		cout << "Error during binding!" << endl;
		system("pause");
		exit(1);
	}
}

void connectSocket(SOCKET sock, string host, string port)
{
	addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	addrinfo* addr = nullptr;
	int errorCode = getaddrinfo(host.c_str(), port.c_str(), &hints, &addr);
	if (errorCode != 0)
	{
		cout << "Error during getting address!" << endl;
		system("pause");
		exit(1);
	}
	errorCode = connect(sock, addr->ai_addr, addr->ai_addrlen);
	if (errorCode != 0)
	{
		cout << "Error during connection attempt!" << endl;
		system("pause");
		exit(1);
	}
}

template <typename T>

pair<bool, T> readData(SOCKET sock)
{
	T result;
	int bytes = recv(sock, (char*)&result, sizeof(result), 0);
	if (bytes == 0)
		return make_pair(true, result);
	else if (bytes < 0)
	{
		cout << "ERROR" << endl;
		return make_pair(true, result);
	}
	else return make_pair(false, result);
}

template <typename T>

bool writeData(SOCKET sock, T data)
{
	int bytes = send(sock, (char*)&data, sizeof(data), 0);
	if (bytes < 0)
	{
		cout << "ERROR" << endl;
		return true;
	}
	else return bytes == 0;
}

const int BUFFER_SIZE = 2048;

pair<bool, string> readString(SOCKET sock)
{
	pair<bool, int> size = readData<int>(sock);
	if (size.first) return make_pair(true, "");
	if (size.second == 0) return make_pair(false, "");
	char* buffer = new char[size.second + 1];
	int bytes = recv(sock, buffer, size.second, 0);
	if (bytes == 0) return make_pair(true, "");
	else if (bytes < 0)
	{
		cout << "ERROR" << endl;
		return make_pair(true, "");
	}
	else
	{
		buffer[size.second] = '\0';
		return make_pair(false, string(buffer));
	}
}

bool writeString(SOCKET sock, string data)
{
	if (writeData<int>(sock, data.length())) return true;
	if (data.length() == 0) return false;
	int bytes = send(sock, data.c_str(), data.length(), 0);
	if (bytes < 0)
	{
		cout << "ERROR" << endl;
		return true;
	}
	else return bytes == 0;
}

enum MessageType
{
	Text,
	SendName,
	PrivateText,
	NameAlreadyUsedError,
	SuccessfulConnection,
	NoNameSuppliedError,
	NoSuchNameError,
	Connected,
	Disconnected,
	RequestNames,
	NameList
};

string connectToServer(SOCKET sock)
{
	while (true)
	{
		cout << "������� ��� ������������: ";
		string username;
		getline(cin, username);
		bool error = writeData<MessageType>(sock, SendName) || writeString(sock, username);
		if (error)
		{
			cout << "������ �����������" << endl;
			exit(1);
		}
		pair<bool, MessageType> type = readData<MessageType>(sock);
		if (type.first)
		{
			cout << "������ �����������" << endl;
			exit(1);
		}
		if (type.second = SuccessfulConnection)
			return username;
		if (type.second = NameAlreadyUsedError)
		{
			readString(sock);
			cout << "��� ��� ��� ������������!" << endl;
			break;
		}
	}
}

CRITICAL_SECTION cs;
string username;
char currentInput[BUFFER_SIZE];
int inputLen;
int inputPos;

void clearLine()
{
	cout << "\r  ";
	for (int i = 0; i < inputLen; i++)
		cout << " ";
	cout << "\r";
}

void print(string s)
{
	EnterCriticalSection(&cs);
	clearLine();
	cout << s << endl << "> " << currentInput;
	LeaveCriticalSection(&cs);
}

string read()
{
	cout << "> ";
	while (true)
	{
		char c = _getch();
		EnterCriticalSection(&cs);
		switch (c)
		{
		case '\r':
		{
			clearLine();
			string s(currentInput);
			cout << "\r��: " << s << endl;
			inputLen = 0;
			inputPos = 0;
			currentInput[0] = '\0';
			LeaveCriticalSection(&cs);
			return s;
		}
		case '\n': break;
		case '\b':
			if (inputPos == 0) break;
			clearLine();
			for (int i = inputPos - 1; i < inputLen; i++)
				currentInput[i] = currentInput[i + 1];
			inputPos--;
			inputLen--;
			break;
		case -32:
		{
			int c2 = _getch();
			if (c2 == 75 && inputPos > 0) inputPos--;
			if (c2 == 77 && inputPos < inputLen) inputPos++;
			break;
		}
		case 0:
			_getch();
			break;
		default:
			for (int i = inputLen; i > inputPos; i--)
				currentInput[i] = currentInput[i - 1];
			currentInput[inputPos++] = c;
			inputLen++;
			currentInput[inputLen] = '\0';
			break;
		}
		cout << "\r> " << currentInput;
		if (inputPos < inputLen)
		{
			int c3 = currentInput[inputPos];
			currentInput[inputPos] = '\0';
			cout << "\r> " << currentInput;
			currentInput[inputPos] = c3;
		}
		LeaveCriticalSection(&cs);
	}
}

bool handleText(SOCKET sock)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	pair<bool, string> message = readString(sock);
	if (message.first) return true;
	print(name.second + ": " + message.second);
	return false;
}

bool handleStatus(SOCKET sock, MessageType status)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	print(name.second + (status == Connected ? " ����� � ���" : " ����� �� ����"));
	return false;
}

bool handlePrivateText(SOCKET sock)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	pair<bool, string> message = readString(sock);
	if (message.first) return true;
	print(name.second + " (����� ���): " + message.second);
	return false;
}

bool printNameList(SOCKET sock)
{
	pair<bool, int> count = readData<int>(sock);
	if (count.first) return true;
	if (count.second == 0)
	{
		print("����� ������ ��");
		return false;
	}
	for (int i = 0; i < count.second; i++)
	{
		pair<bool, string> name = readString(sock);
		if (name.first) return true;
		print(name.second);
	}
	print("");
	return false;
}


DWORD WINAPI manageMessages(LPVOID lpParameter)
{
	SOCKET sock = *(SOCKET*)lpParameter;
	while (true)
	{
		pair<bool, MessageType> type = readData<MessageType>(sock);
		if (type.first) break;
		bool error = false;
		switch (type.second)
		{
		case Text:
			error = handleText(sock);
			break;
		case PrivateText:
			error = handlePrivateText(sock);
			break;
		case NoNameSuppliedError:
			error = true;
			break;
		case NoSuchNameError:
		{
			pair<bool, string> name = readString(sock);
			if (name.first)
			{
				error = true;
				break;
			};
			print("������������ " + name.second + " �� ����������!");
			break;
		}
		case Connected: case Disconnected:
			error = handleStatus(sock, type.second);
			break;
		case NameList:
			error = printNameList(sock);
			break;
		default:
			error = true;
			break;
		}
		if (error) break;
	}
	closesocket(sock);
	return 0;
}

int main()
{
	srand(time(0));
	setlocale(0, "");
	initializeLibrary();
	InitializeCriticalSection(&cs);
	currentInput[0] = '\0';
	inputLen = 0;
	inputPos = 0;
	SOCKET sock = createSocket();
	bindSocket(sock);
	cout << "������� ����� �������: ";
	string host;
	getline(cin, host);
	cout << "������� ���� �������: ";
	string port;
	getline(cin, port);
	connectSocket(sock, host, port);
	username = connectToServer(sock);
	cout << endl;
	CreateThread(NULL, 0, &manageMessages, &sock, 0, NULL);
	while (true)
	{
		string msg = read();
		if (msg == "!names")
		{
			writeData<MessageType>(sock, RequestNames);
			continue;
		}
		regex r("@(\\w+)\\s+(.+)");
		smatch parts;
		if (!regex_match(msg, parts, r))
		{
			writeData<MessageType>(sock, Text);
			writeString(sock, "");
			writeString(sock, msg);
		}
		else
		{
			writeData<MessageType>(sock, PrivateText);
			writeString(sock, parts[1]);
			writeString(sock, parts[2]);
		}
	}
}