#include <iostream>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <string>
#include <map>

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

void initializeLibrary()
{
	WSADATA wsaData;
	int errorCode = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (errorCode != 0)
	{
		cout << "Error during initialization!" << endl;
		system("pause");
		exit(1);
	}
}

SOCKET createSocket()
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		cout << "Error during socket creation!" << endl;
		system("pause");
		exit(1);
	}
	return sock;
}

void bindSocket(SOCKET sock)
{
	addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	addrinfo* addr = nullptr;
	int errorCode = getaddrinfo(0, "1234", &hints, &addr);
	if (errorCode != 0)
	{
		cout << "Error during getting address!" << endl;
		system("pause");
		exit(1);
	}
	errorCode = bind(sock, addr->ai_addr, addr->ai_addrlen);
	if (errorCode != 0)
	{
		cout << "Error during binding!" << endl;
		system("pause");
		exit(1);
	}
}
SOCKET acceptConnection(SOCKET listeningSock)
{
	sockaddr_in clientAddress;
	int clientAddressSize = sizeof(clientAddress);
	SOCKET result = accept(listeningSock, (sockaddr*)&clientAddress, &clientAddressSize);
	return result;
}

template <typename T>
pair<bool, T> readData(SOCKET sock)
{
	T result;
	int bytes = recv(sock, (char*)&result, sizeof(result), 0);
	if (bytes == 0) return make_pair(true, result);
	else if (bytes < 0)
	{
		return make_pair(true, result);
	}
	else return make_pair(false, result);
}

template <typename T>
bool writeData(SOCKET sock, T data)
{
	int bytes = send(sock, (char*)&data, sizeof(data), 0);
	if (bytes < 0)
	{
		return true;
	}
	else return bytes == 0;
}

const int BUFFER_SIZE = 1024;
pair<bool, string> readString(SOCKET sock)
{
	pair<bool, int> size = readData<int>(sock);
	if (size.first) return make_pair(true, "");
	if (size.second == 0) return make_pair(false, "");
	char* buffer = new char[size.second + 1];
	int bytes = recv(sock, buffer, size.second, 0);
	if (bytes == 0) return make_pair(true, "");
	else if (bytes < 0)
	{
		return make_pair(true, "");
	}
	else
	{
		buffer[size.second] = '\0';
		return make_pair(false, string(buffer));
	}
}

bool writeString(SOCKET sock, string data)
{
	if (writeData<int>(sock, data.length())) return true;
	if (data.length() == 0) return false;
	int bytes = send(sock, data.c_str(), data.length(), 0);
	if (bytes < 0)
	{
		return true;
	}
	else return bytes == 0;
}

enum MessageType
{
	Text,
	SendName,
	PrivateText,
	NameAlreadyUsedError,
	SuccessfulConnection,
	NoNameSuppliedError,
	NoSuchNameError,
	Connected,
	Disconnected,
	RequestNames,
	NameList
};

map<string, SOCKET> connections;
CRITICAL_SECTION cs;
bool handleNameList(SOCKET sock, const string& username);

bool handleSendName(SOCKET sock, string& username)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	EnterCriticalSection(&cs);
	if (connections.count(name.second))
	{
		LeaveCriticalSection(&cs);
		return writeData<MessageType>(sock, NameAlreadyUsedError) ||
			writeString(sock, name.second);
	}
	if (username == "")
	{
		username = name.second;
		connections.emplace(username, sock);
		handleNameList(sock, username);
	}
	LeaveCriticalSection(&cs);
	return writeData<MessageType>(sock, SuccessfulConnection);
}

bool handleText(SOCKET sock, const string& username)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	pair<bool, string> message = readString(sock);
	if (message.first) return true;
	if (username == "")
		return writeData<MessageType>(sock, NoNameSuppliedError);
	EnterCriticalSection(&cs);
	for (auto i = connections.begin(); i != connections.end(); i++)
	{
		if (i->first == username) continue;
		writeData(i->second, Text);
		writeString(i->second, username);
		writeString(i->second, message.second);
	}
	LeaveCriticalSection(&cs);
	return false;
}

bool handlePrivateText(SOCKET sock, const string& username)
{
	pair<bool, string> name = readString(sock);
	if (name.first) return true;
	pair<bool, string> message = readString(sock);
	if (message.first) return true;
	if (username == "")
		return writeData<MessageType>(sock, NoNameSuppliedError);
	EnterCriticalSection(&cs);
	if (connections.count(name.second))
	{
		SOCKET recepient = connections[name.second];
		LeaveCriticalSection(&cs);
		return writeData(recepient, PrivateText) ||
			writeString(recepient, username) ||
			writeString(recepient, message.second);
	}
	else
	{
		LeaveCriticalSection(&cs);
		return writeData(sock, NoSuchNameError) ||
			writeString(sock, username);
	}
}

void sendStatus(const string& username, MessageType status)
{
	EnterCriticalSection(&cs);
	bool error = false;
	for (auto i = connections.begin(); i != connections.end(); i++)
	{
		if (i->first == username) continue;
		writeData(i->second, status);
		writeString(i->second, username);
	}
	LeaveCriticalSection(&cs);
}

bool handleNameList(SOCKET sock, const string& username)
{
	bool error = writeData(sock, NameList);
	error = error || writeData<int>(sock, connections.size() - 1);
	for (auto i = connections.begin(); i != connections.end(); i++)
	{
		if (i->first == username) continue;
		error = error || writeString(sock, i->first);
	}
	return error;
}

DWORD WINAPI manageClient(LPVOID lpParameter)
{
	SOCKET sock = *(SOCKET*)lpParameter;
	string username = "";
	while (true)
	{
		pair<bool, MessageType> type = readData<MessageType>(sock);
		if (type.first) break;
		bool error = false;
		switch (type.second)
		{
		case SendName:
			error = handleSendName(sock, username);
			if (username != "")
			{
				sendStatus(username, Connected);
			}
			break;
		case Text:
			error = handleText(sock, username);
			break;
		case PrivateText:
			error = handlePrivateText(sock, username);
			break;
		case RequestNames:
			error = handleNameList(sock, username);
			break;
		default:
			error = true;
			break;
		}
		if (error) break;
	}
	EnterCriticalSection(&cs);
	if (username != "")
	{
		sendStatus(username, Disconnected);
		connections.erase(username);
	}
	LeaveCriticalSection(&cs);
	closesocket(sock);
	return 0;
}

int main()
{
	initializeLibrary();
	InitializeCriticalSection(&cs);
	SOCKET listeningSock = createSocket();
	bindSocket(listeningSock);
	listen(listeningSock, 3);
	while (true)
	{
		SOCKET clientSock = acceptConnection(listeningSock);
		cout << "New connection!" << endl;
		CreateThread(NULL, 0, &manageClient, &clientSock, 0, NULL);
	}
}